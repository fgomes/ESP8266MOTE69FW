

#include <ESP8266MOTE69FW.h>


bool HandleSerialHandshake(RFM69 radio, byte targetID, boolean isEOF, uint16_t MsgTimeout, uint16_t AckTimeout, bool dbgOn)
{
  long now = millis();

  while (millis()-now<MsgTimeout)
  {
    if(dbgOn) Serial.print(".");
    if (radio.sendWithRetry(targetID, isEOF ? "FLX?EOF" : "FLX?", isEOF?7:4, 2,AckTimeout)) {
      if(dbgOn) {
        radio.DATA[radio.DATALEN] = 0;
        Serial.print("Reply: ");
        Serial.println((char*)radio.DATA);
      }
      if (radio.DATALEN == 6 && radio.DATA[0]=='F' && radio.DATA[1]=='L' && radio.DATA[2]=='X' && 
          radio.DATA[3]=='?' && radio.DATA[4]=='O' && radio.DATA[5]=='K') {
        Serial.println("R:OK");
        return true;
      }
      if (radio.DATALEN == 7 && radio.DATA[0]=='F' && radio.DATA[1]=='L' && radio.DATA[2]=='X' && 
          radio.DATA[3]=='?' && radio.DATA[4]=='N' && radio.DATA[5]=='O' && radio.DATA[6]=='K') {
        Serial.println("R:NOK");
        return true;
      }
    } else {
      Serial.println("No ACK");
    }
    yield();
  }

  if (dbgOn) Serial.println("Handshake fail");
  return false;
}


bool CheckForFileHEX(char* fileName, RFM69 radio, byte targetID, uint16_t MsgTimeout, uint16_t AckTimeout, bool dbgOn)
{
  char* pStr;
  byte sendBuf[57];
  int seq = 0;
  
  if (HandleSerialHandshake(radio, targetID, false, MsgTimeout, AckTimeout, dbgOn)) {
    // Handshake ok, start sending hex
    Serial.println("Start to send HEX");
    
    File f = SPIFFS.open(fileName, "r");
    if (!f) {
      Serial.println("File open failed");
      f.close();
      return(false);
    }
    Serial.print("File size: "); Serial.println(f.size());
    while(f.available()) {

/* Multi record */
      byte tmpSize = 0;
      char tmpBuf[100];
      for(uint8_t i=0; i<3; i++) {
        if(!f.available())
          break;
        // Lets read line by line from the file
        String line = f.readStringUntil('\n');
        pStr = (char*)line.c_str();
        Serial.println(pStr);
        byte hexSize = BYTEfromHEX(pStr[1], pStr[2]);
        memcpy(tmpBuf+(tmpSize*2), pStr+9, hexSize*2);
        tmpSize += hexSize;
      }
      Serial.println(tmpBuf);
      byte sendBufLen = prepareSendBuffer(tmpBuf, sendBuf, tmpSize, seq);
      Serial.print("sendBufLen: "); Serial.println(sendBufLen);
      if(sendHEXPacket(radio, targetID, sendBuf, sendBufLen, seq, MsgTimeout, AckTimeout, dbgOn)) {
        sprintf((char*)sendBuf, "FLX:%u:OK",seq);
        Serial.println((char*)sendBuf); //response to host (python?)
        seq++;
      } else {
        return false;
      }


/* Single record *
      // Lets read line by line from the file
      String line = f.readStringUntil('\n');
      Serial.println(line);
      pStr = (char*)line.c_str();
      byte hexSize = BYTEfromHEX(pStr[1], pStr[2]);
      if(hexSize == 0) 
        break;
      Serial.print("hexSize: "); Serial.println(hexSize);
      pStr += 9; // Skip initial intel hex header ':SSAAAATT' and jump to raw data assuming all the addresses are contiguous
      // Send it to the node:
      byte sendBufLen = prepareSendBuffer(pStr, sendBuf, hexSize, seq);
      Serial.print("sendBufLen: "); Serial.println(sendBufLen);
      if(sendHEXPacket(radio, targetID, sendBuf, sendBufLen, seq, MsgTimeout, AckTimeout, dbgOn)) {
        sprintf((char*)sendBuf, "FLX:%u:OK",seq);
        Serial.println((char*)sendBuf); //response to host (python?)
        seq++;
      } else {
        return false;
      }
***********************/
      
    }
    f.close();
    if (HandleSerialHandshake(radio, targetID, true, MsgTimeout, AckTimeout, dbgOn)) {
      return true;
    } else {
      if(dbgOn) Serial.println("EOF handshake not ok");   
      return false;
    }
    return true;
  } else {
    if(dbgOn) Serial.println("Initial handshake not ok");
    return false;
  }
}

//returns the final size of the buf
byte prepareSendBuffer(char* hexdata, byte*buf, byte length, uint16_t seq)
{
  byte seqLen = sprintf(((char*)buf), "FLX:%u:", seq);
  for (byte i=0; i<length;i++)
    buf[seqLen+i] = BYTEfromHEX(hexdata[i*2], hexdata[i*2+1]);
  return seqLen+length;
}

//assume A and B are valid HEX chars [0-9A-F]
byte BYTEfromHEX(char MSB, char LSB)
{
  return (MSB>=65?MSB-55:MSB-48)*16 + (LSB>=65?LSB-55:LSB-48);
}

//return the SEQ of the ACK received, or -1 if invalid
boolean sendHEXPacket(RFM69 radio, byte targetID, byte* sendBuf, byte hexDataLen, uint16_t seq, uint16_t MsgTimeout, uint16_t AckTimeout, boolean dbgOn)
{
  long now = millis();
  
  while(1) {
    if (dbgOn) { Serial.print("RFTX > "); PrintHex83(sendBuf, hexDataLen); }
    if (radio.sendWithRetry(targetID, sendBuf, hexDataLen, 2, AckTimeout))
    {
      byte ackLen = radio.DATALEN;
      
      if (dbgOn) { Serial.print("RFACK > "); Serial.print(ackLen); Serial.print(" > "); PrintHex83((byte*)radio.DATA, ackLen); }
      
      if (ackLen >= 8 && radio.DATA[0]=='F' && radio.DATA[1]=='L' && radio.DATA[2]=='X' && 
          radio.DATA[3]==':' && radio.DATA[ackLen-3]==':' &&
          radio.DATA[ackLen-2]=='O' && radio.DATA[ackLen-1]=='K')
      {
        uint16_t tmp=0;
        // sscanf((const char*)radio.DATA, "FLX:%u:OK", &tmp);
        tmp = atoi((const char*)&(radio.DATA[4]));
        return tmp == seq;
      }
    }

    if (millis()-now > MsgTimeout)
    {
      Serial.println("Timeout waiting for packet ACK, aborting FLASH operation ...");
      break; //abort FLASH sequence if no valid ACK was received for a long time
    }
    yield();
  }
  return false;
}

void PrintHex83(uint8_t *data, uint8_t length) // prints 8-bit data in hex
{
  char tmp[length*2+1];
  byte first ;
  int j=0;
  for (uint8_t i=0; i<length; i++) 
  {
    first = (data[i] >> 4) | 48;
    if (first > 57) tmp[j] = first + (byte)39;
    else tmp[j] = first ;
    j++;

    first = (data[i] & 0x0F) | 48;
    if (first > 57) tmp[j] = first + (byte)39; 
    else tmp[j] = first;
    j++;
  }
  tmp[length*2] = 0;
  Serial.println(tmp);
}
