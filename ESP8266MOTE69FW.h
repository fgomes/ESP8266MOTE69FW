#ifndef _ESP8266MOTE69FW_H_
#define _ESP8266MOTE69FW_H_

#include <RFM69.h>          //get it here: https://www.github.com/lowpowerlab/rfm69
//#include <SPI.h>
#include <FS.h>

#define ACK_TIME    50  // # of ms to wait for an ack
#define TIMEOUT     3000

bool HandleSerialHandshake(RFM69 radio, byte targetID, boolean isEOF, uint16_t MsgTimeout, uint16_t AckTimeout, bool dbgOn);
bool CheckForFileHEX(char* fileName, RFM69 radio, byte targetID, uint16_t MsgTimeout=TIMEOUT, uint16_t AckTimeout=ACK_TIME, bool dbgOn=false);
byte prepareSendBuffer(char* hexdata, byte*buf, byte length, uint16_t seq);
byte BYTEfromHEX(char MSB, char LSB);
boolean sendHEXPacket(RFM69 radio, byte targetID, byte* sendBuf, byte hexDataLen, uint16_t seq, uint16_t MsgTimeout, uint16_t AckTimeout, boolean dbgOn);
void PrintHex83(uint8_t *data, uint8_t length);

#endif
